﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game : MonoBehaviour {
    public Image prefab;
    public Image image;
    public RectTransform keyborad;
    public Sprite[] logos;
    public string nextLevel;
    

    private int level = -1;

    private void Awake() {
        NextLevel.nextLevel = nextLevel;
        Initialize();
    }
     
    public void Initialize() {
        level++;
       // SoundManager.PlaySound("Background");

        if (level < logos.Length ) {
            SpawnName();
            SpawnLogo();
            ResetKeyboard();
        } else {
            SceneManager.LoadScene("WinningScene");
            //Debug.Log("Win");
            // TODO: What happens when you win?
      
            // TODO: Can you rearrange the letters to be ABCDE instead of QWERTY
        }
    }

    private void ResetKeyboard() {
        foreach (Button b in keyborad.GetComponentsInChildren<Button>()) {
            Debug.Log(b.transform.GetChild(0).GetComponent<Text>().text);
            b.interactable = true;
        }
    }

    private void SpawnLogo() {
        image.sprite = logos[level];
    }

    private void SpawnName() {
        CleanLetters();

        List<Image> letters = new List<Image>();

        foreach (char c in logos[level].name) {
            Debug.Log(c);

            Image image = Instantiate(prefab, transform);

            Transform child = image.transform.GetChild(0);
            Text childText = child.GetComponent<Text>();
            childText.text = c.ToString();
            childText.enabled = false;

            letters.Add(image);
        }

        for (int i = 0; i < 3; i++) {
            int random = UnityEngine.Random.Range(0, letters.Count);
            Image image = letters[random];
            image.color = new Color (0.7f,0.7f,0.7f);
            Transform child = image.transform.GetChild(0);
            child.GetComponent<Text>().enabled = true;

        }
        
        // TODO: Re-enable keyboard letters when you clear a stage

    }

    private void CleanLetters() {
        for (int i = 0; i < transform.childCount; i++) {
            Destroy(transform.GetChild(i).gameObject);
        }
    }
}


