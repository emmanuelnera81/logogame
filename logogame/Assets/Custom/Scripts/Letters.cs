﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;

public class Letters : MonoBehaviour, IPointerClickHandler {
    public static int guesses;
    
    private void Awake() {
        guesses = 0;

    }

    public void OnPointerClick(PointerEventData eventData) {
        if (!GetComponent<Button>().interactable) { return; }

        string s = transform.GetChild(0).GetComponent<Text>().text;
        Debug.Log(s);

        //get acess to your canvas group on this button, and disable "interactable".
        GetComponent<Button>().interactable = false;

        //get access to your game component
        Game game = FindObjectOfType<Game>();

        //store a reference to the text components found in the children of your "game".
        Text[] texts = game.transform.GetComponentsInChildren<Text>();

        //loop through the text elements and ask if its identical to the button you pressed.
        bool isCorrectEntry = false;
        foreach (Text t in texts) {
            if (t.text.ToUpper() == s.ToUpper()) {
                Debug.Log("Correct");
               // Right.Play();
                // TODO: Reveal the matching letter and fade the background to grey.
                //GetComponent<CanvasGroup>().GetComponentsInChildren<Image>().enabled = true;
                if (!t.enabled) {
                    isCorrectEntry = true;
                    SoundManager.PlaySound("Right");
                }
                
                t.enabled = true;
                Image img = t.transform.parent.GetComponent<Image>();
                img.color = new Color(0.7f, 0.7f, 0.7f);
             
            }

        }

        if (!isCorrectEntry) {
            Debug.Log("Not Correct");
            SoundManager.PlaySound("Wrong");
            guesses++;
            
            if (guesses == 3) {
                SceneManager.LoadScene("LosingScene");
            }

            ScoreUI.Instance.SetScoreText(guesses);
        }

        // TODO: Iterate through every letter, if none of the letters are hidden anymore, we must have won.
        CheckForVictory();
    }

    // Declare a method that will...
    //...iterate through your game's text children
    void CheckForVictory() {
        foreach (Text t in FindObjectOfType<Game>().transform.GetComponentsInChildren<Text>()) {
            //and ask if text is enabled
            if (!t.enabled) {
            //if it is... return...
                return;
            }
        }

        Win();
    }
    //if we make it the whole way through without returning, we can run some victory code.
    //declare a win method
    //not sure what it does up to you
    public void Win() {
        // show some victory UI
        // load next logo.

        FindObjectOfType<Game>().Initialize();    
       
        // check if all logos are done (victories > logoCount)
        // congratulations and back to start screen
    }

}
