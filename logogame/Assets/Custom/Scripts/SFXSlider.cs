﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SFXSlider : MonoBehaviour { 
   public static float volume = 1;

private Slider sfxSlider;

private void Awake() {
    sfxSlider = GetComponent<Slider>();

    sfxSlider.value = volume;
    sfxSlider.onValueChanged.AddListener((e) => {
        Debug.Log(e);
        volume = sfxSlider.value;
    });
}

}
