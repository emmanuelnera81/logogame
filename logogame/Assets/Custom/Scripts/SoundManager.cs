﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour {
    static AudioSource audioSrc;

    public static AudioClip RightSound, WrongSound;
    public Slider MusicSlider;

    void Start() {

        RightSound = Resources.Load<AudioClip>("Sound/Right");
        //Debug.Log(RightSound);
        WrongSound = Resources.Load<AudioClip>("Sound/Wrong");
        //Debug.Log(WrongSound);
       
        

        audioSrc = GetComponent<AudioSource>();
    }

    void Update() {
        //audioSrc.volume = MusicSlider.value;
        
    }

    public static void PlaySound(string clip) {
        //Debug.Log(" ================================= " + clip);
        switch (clip) {
            case "Right":
                audioSrc.PlayOneShot(RightSound);
                break;
            case "Wrong":
                audioSrc.PlayOneShot(WrongSound);
                break;

        }
    }
}
