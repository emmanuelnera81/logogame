﻿using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevel : MonoBehaviour, IPointerClickHandler {
    public static string nextLevel;

    public void OnPointerClick(PointerEventData eventData) {
        SceneManager.LoadScene(nextLevel);
    }
}
