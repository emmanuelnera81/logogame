﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreUI : MonoBehaviour {
    public static ScoreUI Instance;
    
    public Text text;

    private void Awake() {
        Instance = this;
    }

    public void SetScoreText(int guesses) {
        text.text = "Strikes: " + guesses;

    }
}

