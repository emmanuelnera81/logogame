﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicSlider : MonoBehaviour {
    public static float volume = 1;
    
    private Slider musicSlider;

    private void Awake() {
        musicSlider = GetComponent<Slider>();

        musicSlider.value = volume;
        musicSlider.onValueChanged.AddListener((e) => {
            Debug.Log(e);
            volume = musicSlider.value;
        });
    }

}

